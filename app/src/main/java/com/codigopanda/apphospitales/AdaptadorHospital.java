package com.codigopanda.apphospitales;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by victor on 12/15/18.
 */

public class AdaptadorHospital extends BaseAdapter{
    List<Hospital> lista;
    Context context;
    LayoutInflater l;

    public AdaptadorHospital(List<Hospital> li, Context cx){
        this.lista=li;
        this.context = cx;
        this.l = (LayoutInflater)
                cx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class Holder{
        ImageView image;
        TextView titulo;
        TextView descripcion;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = l.inflate(R.layout.item_hospital,null);
        Holder h = new Holder();
        h.image =(ImageView) v.findViewById(R.id.ItemImage);
        h.titulo = (TextView) v.findViewById(R.id.ItemTitle);
        h.descripcion = (TextView) v.findViewById(R.id.ItemDescription);


        Hospital hos = lista.get(position);
        h.image.setImageResource(hos.imagen);
        h.titulo.setText(hos.nombre);
        h.descripcion.setText(hos.descripcion);


        return v;
    }
}
