package com.codigopanda.apphospitales;

import java.util.List;

/**
 * Created by victor on 12/15/18.
 */

public class Hospital {


    public String nombre;
    public String direccion;
    public String telefono;
    public String descripcion;
    public int imagen;
    public String latitud;
    public String longitud;

    public Hospital(String nombre, int imagen, String descripcion) {
        this.nombre = nombre;
        this.imagen = imagen;
        this.descripcion = descripcion;
    }

    List<Especialidad> listaEspecialidades;


    public Hospital(String direccion, String telefono, String descripcion, int imagen, String latitud, List<Especialidad> listaEspecialidades, String longitud, String nombre) {
        this.direccion = direccion;
        this.telefono = telefono;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.latitud = latitud;
        this.listaEspecialidades = listaEspecialidades;
        this.longitud = longitud;
        this.nombre = nombre;
    }

}
