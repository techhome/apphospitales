package com.codigopanda.apphospitales;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class clsMain extends AppCompatActivity {
    ListView listaHospitales;
    AdaptadorHospital adapHospital;
    List<Hospital> lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fmain);
        listaHospitales= (ListView) findViewById(R.id.lista);
        lista = new ArrayList<>();
        this.CargarHospitales();
        adapHospital = new AdaptadorHospital(lista,clsMain.this);
        listaHospitales.setAdapter(adapHospital);
        listaHospitales.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GLobalDatos.hospital = lista.get(position);
                startActivity(new Intent(clsMain.this,clsDetalleHospital.class));
            }
        });
    }
    public void CargarHospitales(){
       lista.add(new Hospital("Hospital de Ninos",
               R.drawable.fotomar,"descripcion"));
    }
}
