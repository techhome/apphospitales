package com.codigopanda.apphospitales;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class clsDetalleHospital extends AppCompatActivity {
    TextView prueba;
    ImageView imagePrueba;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fdetalle_hospital);
        prueba = (TextView) findViewById(R.id.prueba);
        prueba.setText(GLobalDatos.hospital.nombre);
        imagePrueba = (ImageView) findViewById(R.id.imagePrueba);
        imagePrueba.setImageResource(GLobalDatos.hospital.imagen);
    }
}
